package co.com.ies.pruebas.webservice.redis;

public interface DistributedServiceProcess {
    void process();

}
