package co.com.ies.pruebas.webservice;

import com.ibm.cuda.CudaDevice;

import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public abstract class QueueAsyncAbstract<TasckType> {

    protected abstract void offer(TasckType element);
    protected abstract Set<TasckType> getQueue();
    protected abstract void processElement(TasckType element);

    public void offerTascks(List<TasckType> elements){
        //agregar a la cola
        elements.forEach(this::offer);

    }

    public void offerTasck(TasckType element){
        //agregar a la cola
        offer(element);

    }

    public void processQueue(){
        Set<TasckType> elements = getQueue();
        System.out.println("QueueAsyncAbstract.processQueue elements = " + elements.size());
        final Iterator<TasckType> iterator = elements.iterator();

        while (iterator.hasNext()){
            final TasckType tasckType = iterator.next();
            try {
                processElement(tasckType);
                elements.remove(tasckType);
                iterator.remove();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error al procesar la tarea, se agrega de nuevo en la cola");
                offer(tasckType);
            }
        }


    }

    private void packProcess(int limit) {
        Set<TasckType> elementsPack = getQueue();
        System.out.println("QueueAsyncAbstract.processQueue elementsPack = " + elementsPack.size());
        // use un iterator en eves del for y un poll por que me quedaban completas las tareas
        final Iterator<TasckType> iterator = elementsPack.iterator();
        int count = 0;
        while (iterator.hasNext() && count < limit){
            final TasckType tasckType = iterator.next();
            try {
                processElement(tasckType);
                elementsPack.remove(tasckType);
                iterator.remove();
                count++;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error al procesar la tarea, se agrega de nuevo en la cola");
                offer(tasckType);
            }
        }
    }


}
