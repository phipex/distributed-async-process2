package co.com.ies.pruebas.webservice.redis;

import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

@Component
public class DistributedQeueuServiceImpl implements DistributedQeueuService{

    private final QeueuAsyncRedis qeueuAsyncRedis;
    private final RedissonClient redissonClient;

    public DistributedQeueuServiceImpl(QeueuAsyncRedis qeueuAsyncRedis, RedissonClient redissonClient) {
        this.qeueuAsyncRedis = qeueuAsyncRedis;
        this.redissonClient = redissonClient;
    }

    @Override
    public void process() {

        RSemaphore semaphore = redissonClient.getSemaphore("mySemaphore");
        final int availablePermits = semaphore.availablePermits();

        if(availablePermits == 0){
            final boolean trySetPermits = semaphore.trySetPermits(1);
            System.out.println("DistributedQeueuServiceImpl.process trySetPermits" + trySetPermits);
        }
        final boolean tryAcquire = semaphore.tryAcquire();
        System.out.println("DistributedQeueuServiceImpl.process " + tryAcquire + " availablePermits = " + availablePermits);
        if(tryAcquire){
            System.out.println("DistributedQeueuServiceImpl.process adquire");
            qeueuAsyncRedis.processQueue();
            semaphore.release();
            System.out.println("DistributedQeueuServiceImpl.process release");
        }

    }
}
