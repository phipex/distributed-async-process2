package co.com.ies.pruebas.webservice.redis;

import co.com.ies.pruebas.webservice.Greeting;
import co.com.ies.pruebas.webservice.QueueAsyncAbstract;
import org.redisson.api.*;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class QeueuAsyncRedis  extends QueueAsyncAbstract<Greeting> {

    private static final String KEY_QEUEU = "Pending.TaskTest_Qeueu";

    private final RedissonClient redissonClient;
    private final ProcessorDelayedRedis processorDelayed;

    public QeueuAsyncRedis(RedissonClient redissonClient, ProcessorDelayedRedis processorDelayed) {
        this.redissonClient = redissonClient;
        this.processorDelayed = processorDelayed;
    }

    @Override
    protected void offer(Greeting element) {

        Set<Greeting> queue = getQueue();
        queue.add(element);

    }

    @Override
    protected Set<Greeting> getQueue() {
        return redissonClient.getSet(KEY_QEUEU);
        //TODO mirar el tema de los listener

    }

    @Override
    protected void processElement(Greeting element) {
        System.out.println("QeueuAsyncRedis.processElement "+ element.getId());
        processorDelayed.processElement(element);

    }
}
