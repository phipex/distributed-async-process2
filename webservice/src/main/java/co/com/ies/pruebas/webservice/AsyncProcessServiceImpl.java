package co.com.ies.pruebas.webservice;

import co.com.ies.pruebas.webservice.redis.DistributedServiceProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncProcessServiceImpl implements AsyncProcessService{
    @Autowired
    @Qualifier(RedisConfig.REMOTE_PROCESS)
    private DistributedServiceProcess serviceProcessQeueu;

    @Async
    @Override
    public void process() {
        serviceProcessQeueu.process();
    }

}
