package co.com.ies.pruebas.webservice.redis;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import co.com.ies.pruebas.webservice.Greeting;
import co.com.ies.pruebas.webservice.GreetingRepository;
import co.com.ies.pruebas.webservice.RedisConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class DistributedServiceProcessImpl implements DistributedServiceProcess {

    private final GreetingRepository greetingRepository;
    private final QeueuAsyncRedis qeueuAsyncRedis;

    @Autowired
    @Qualifier(RedisConfig.REMOTE_PROCESS_QUEUE)
    private DistributedQeueuService distributedQeueuService;

    public DistributedServiceProcessImpl(GreetingRepository greetingRepository, QeueuAsyncRedis qeueuAsyncRedis) {
        this.greetingRepository = greetingRepository;
        this.qeueuAsyncRedis = qeueuAsyncRedis;
    }

    @Async
    @Override
    public void process() {
        final List<Greeting> greetingsByIpTramitedIsNull = greetingRepository.getGreetingsByIpTramitedIsNull();

        qeueuAsyncRedis.offerTascks(greetingsByIpTramitedIsNull);
        final int size = greetingsByIpTramitedIsNull.size();
        System.out.println("size = " + size);
        // TODO Publicar eventos
        distributedQeueuService.process();
    }

}
